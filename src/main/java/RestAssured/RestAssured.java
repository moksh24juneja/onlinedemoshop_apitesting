package RestAssured;

import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;
import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;




public class RestAssured {
    static Response getResponse;
    static int responseCount;

    @Test
    public static Response getPet(){

    getResponse=
         given()
                .header("content-Type","application/json")
                .queryParam("status","available")
        .when()
                .get("https://petstore.swagger.io/v2/pet/findByStatus");


        int statusCode=getResponse.statusCode();
        Assert.assertEquals(statusCode,200);
        responseCount=getResponse.jsonPath().getList("$").size();
          System.out.println(responseCount);
          return getResponse;
    }

    @Test
    public static void addPet(){

      String payload ="{\n" +
                "  \"id\": 0,\n" +
                "  \"category\": {\n" +
                "    \"id\": 0,\n" +
                "    \"name\": \"string\"\n" +
                "  },\n" +
                "  \"name\": \"doggie\",\n" +
                "  \"photoUrls\": [\n" +
                "    \"string\"\n" +
                "  ],\n" +
                "  \"tags\": [\n" +
                "    {\n" +
                "      \"id\": 0,\n" +
                "      \"name\": \"string\"\n" +
                "    }\n" +
                "  ],\n" +
                "  \"status\": \"available\"\n" +
                "}";

       Response response1=  given()
                .header("content-Type","application/json")
                .body(payload)
        .when()
                .post("https://petstore.swagger.io/v2/pet");

        response1.then().assertThat().body("status", is("available"));
        response1.then().assertThat().body("name", is("doggie"));

         int responseCount1=response1.jsonPath().getMap("$").size();

        if (responseCount1 == 1 ){
           System.out.println("New pet successfully added");
        }
    }

    @Test
    public static void updatePetStatus(){

        String payload ="{\n" +
                "  \"id\": 0,\n" +
                "  \"category\": {\n" +
                "    \"id\": 0,\n" +
                "    \"name\": \"string\"\n" +
                "  },\n" +
                "  \"name\": \"doggie\",\n" +
                "  \"photoUrls\": [\n" +
                "    \"string\"\n" +
                "  ],\n" +
                "  \"tags\": [\n" +
                "    {\n" +
                "      \"id\": 0,\n" +
                "      \"name\": \"string\"\n" +
                "    }\n" +
                "  ],\n" +
                "  \"status\": \"sold\"\n" +
                "}";

       Response response1= given()
                .header("content-Type","application/json")
                .body(payload)
                .when()
                .put("https://petstore.swagger.io/v2/pet");

        int statusCode=response1.statusCode();
        Assert.assertEquals(statusCode,200);
        String petStatus=response1.jsonPath().get("status");
        Assert.assertEquals(petStatus,"sold");

        int newPetCount=getResponse.jsonPath().getList("$").size();;

        if (newPetCount == (responseCount) ){
            System.out.println("Pet status successfully updated");
        }
    }


    @Test
    public static void deletePet(){

        long petId=getPet().jsonPath().get("id[1]");
        System.out.println(petId);

        Response response1= given()
                .header("content-Type","application/json")
                .pathParam("petId",petId)
                .when()
                .delete("https://petstore.swagger.io/v2/pet/{petId}");


        int statusCode=response1.statusCode();
        Assert.assertEquals(statusCode,200);
        String responseMessage= String.valueOf(petId);
        String message=response1.jsonPath().get("message");
        Assert.assertEquals(message,responseMessage);

    }
}
