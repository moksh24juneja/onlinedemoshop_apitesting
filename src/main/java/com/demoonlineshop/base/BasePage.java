package com.demoonlineshop.base;



import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;

import com.demoonlineshop.pages.action.TopNavigation;






/**
 * TODO Put here a description of what this class does.
 *
 * @author z003x39f.
 *         Created Oct 16, 2020.
 */
public class BasePage {
	
	
	public static WebDriver driver;
	public static String browser;
	public static WebDriverWait wait;
	public static TopNavigation topNav;
	public static Logger log= Logger.getLogger("devpinoyLogger");
	
	
	
	@BeforeSuite(alwaysRun = true)
	public void setup() {
		
		PropertyConfigurator.configure(System.getProperty("user.dir") + "\\src\\test\\resources\\properties\\log4j.properties");
	}

	@BeforeMethod(alwaysRun =true)
	public void setUpSelenium() {
		browser="Chrome";
		
		if(browser.toLowerCase().contains("firefox")) {
        	driver=new FirefoxDriver();
        	log.debug("Launching Firefox");
        	
		    }
		
		    if(browser.toLowerCase().contains("chrome")) {
        	System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir") + "\\src\\test\\resources\\executables\\chromedriver.exe");
        
			ChromeOptions options = new ChromeOptions();
			options.setExperimentalOption("useAutomationExtension", false);
			//options.setExperimentalOption("prefs", prefs);
			options.addArguments("--disable-extensions");
			options.addArguments("--disable-infobars");
        	
        	driver = new ChromeDriver(options); 
        	log.debug("Launching chrome");
        	
		    }
		    
        	if (browser.toLowerCase().contains("ie")) {
        	System.setProperty("webdriver.ie.driver",System.getProperty("user.dir") + "\\src\\test\\resources\\executables\\ie.exe" );
        	driver=new InternetExplorerDriver();
        	log.debug("Launching InternetExplorer");
        	
        	}

        	driver.get("https://www.demoblaze.com/index.html");
    		driver.manage().window().maximize();
    		
    		topNav = new TopNavigation(driver);
		
	}
	
	@AfterMethod
	public void tearDown() {
      //  driver.quit();
	}
	
	@AfterTest
	public void closeSelenium() {
		//driver.close();
	}
	
	
	
	public static void click(WebElement element) {
		element.click();
		log.debug("Clicking on an Element : "+element);
	}
	
	public static void type(WebElement element, String value) {
		System.out.println(value);
		element.sendKeys(value);
		log.debug("Typing in an Element : "+element+" entered value as : "+value);
	}

	
}
