package com.demoonlineshop.pages.locators;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * TODO Put here a description of what this class does.
 *
 * @author z003x39f.
 *         Created Oct 16, 2020.
 */
public class CartPageLocator {
	
	
	@FindBy(xpath="(//a[contains(text(),'Delete')])[1]")
	public WebElement Delete_LaptopDelli78gb;
	
	
	@FindBy(xpath="//button[contains(text(),'Place Order')]")
	public WebElement PlaceOrder;
	
	@FindBy(xpath="//*[@id='name']")
	public WebElement PlaceOrderForm_Name;
	
	@FindBy(xpath="//*[@id='country']")
	public WebElement PlaceOrderForm_Country;
	
	@FindBy(xpath="//*[@id='city']")
	public WebElement PlaceOrderForm_City;
	
	@FindBy(xpath="//*[@id='card']")
	public WebElement PlaceOrderForm_CreditCard;
	
	@FindBy(xpath="//*[@id='month']")
	public WebElement PlaceOrderForm_Month;
	
	@FindBy(xpath="//*[@id='year']")
	public WebElement PlaceOrderForm_Year;
	
	@FindBy(xpath="//button[contains(text(),'Purchase')]")
	public WebElement Purchase;

}
