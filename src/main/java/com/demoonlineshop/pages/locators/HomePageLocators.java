package com.demoonlineshop.pages.locators;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * TODO Put here a description of what this class does.
 *
 * @author z003x39f.
 *         Created Oct 16, 2020.
 */
public class HomePageLocators {
	
	@FindBy(xpath="//a[contains(text(),'Phones')]")
	public WebElement PhonesTab;
	
	
	@FindBy(xpath="//a[contains(text(),'Monitors')]")
	public WebElement MonitorTab;
	
	@FindBy(xpath="//a[contains(text(),'Laptops')]")
	public WebElement LaptopTab;

	@FindBy(xpath="//a[contains(text(),'Sony vaio i5')]")
	public WebElement SonyVaioi5;
	
	@FindBy(xpath="//a[contains(text(),'Dell i7 8gb')]")
	public WebElement Delli78gb;
	
	
}
