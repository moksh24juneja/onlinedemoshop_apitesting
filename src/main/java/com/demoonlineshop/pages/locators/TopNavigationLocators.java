package com.demoonlineshop.pages.locators;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * TODO Put here a description of what this class does.
 *
 * @author z003x39f.
 *         Created Oct 16, 2020.
 */
public class TopNavigationLocators {
	
	@FindBy(xpath="//a[contains(text(),'Home')]")
	public WebElement Home;
	
	
	@FindBy(xpath="//a[contains(text(),'Cart')]")
	public WebElement Cart;
	
	

}
