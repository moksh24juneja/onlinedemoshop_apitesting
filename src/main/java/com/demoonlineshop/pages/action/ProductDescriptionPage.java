package com.demoonlineshop.pages.action;


import org.openqa.selenium.Alert;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.testng.Assert;

import com.demoonlineshop.base.BasePage;
import com.demoonlineshop.pages.locators.ProductDescriptionLocators;

/**
 * TODO Put here a description of what this class does.
 *
 * @author z003x39f.
 *         Created Oct 16, 2020.
 */
public class ProductDescriptionPage extends BasePage {

	
	ProductDescriptionLocators productDescription=null;
	
	ProductDescriptionPage(){
		this.productDescription=new ProductDescriptionLocators();
		AjaxElementLocatorFactory factory = new AjaxElementLocatorFactory(driver,10);
		PageFactory.initElements(factory, this.productDescription);
	}
	
	
	 public ProductDescriptionPage AddToCart(){
			click(productDescription.AddToCart);
			return this;
		  }
	 
	 public TopNavigation clickOKPopUp() {
		   try {
			Thread.sleep(2000);
		} catch (InterruptedException exception) {
			// TODO Auto-generated catch-block stub.
			exception.printStackTrace();
		}
		 
		   Alert alert= driver.switchTo().alert();
		   System.out.println( alert.getText());
		   alert.accept();
		   return new TopNavigation(driver);
		  }
}
