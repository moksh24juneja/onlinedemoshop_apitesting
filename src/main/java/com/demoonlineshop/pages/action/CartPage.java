package com.demoonlineshop.pages.action;

import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

import com.demoonlineshop.base.BasePage;
import com.demoonlineshop.pages.locators.CartPageLocator;
import com.demoonlineshop.pages.locators.HomePageLocators;

/**
 * TODO Put here a description of what this class does.
 *
 * @author z003x39f.
 *         Created Oct 16, 2020.
 */
public class CartPage extends BasePage {
	
public CartPageLocator cart;
	
	public CartPage() {
		this.cart=new CartPageLocator();
		AjaxElementLocatorFactory factory = new AjaxElementLocatorFactory(driver,10);
		PageFactory.initElements(factory, this.cart);
	}
	
	public CartPage deleteTheProduct_LaptopDelli78gb() {
		//Currently deleting directly otherwise can implement WebTable
		click(cart.Delete_LaptopDelli78gb);
		try {
			Thread.sleep(4000);
		} catch (InterruptedException exception) {
			// TODO Auto-generated catch-block stub.
			exception.printStackTrace();
		}
		return this;	
	}

	public CartPage clickOnPlaceOrder() {
		click(cart.PlaceOrder);
		return this;	
	}
	
	public CartPage FillPurchaseOrderForm() {
	    try {
			Thread.sleep(2000);
		} catch (InterruptedException exception) {
			// TODO Auto-generated catch-block stub.
			exception.printStackTrace();
		}
		type(cart.PlaceOrderForm_Name, "Moksh");
		type(cart.PlaceOrderForm_Country,"India");
		type(cart.PlaceOrderForm_City,"Dehradun");
		type(cart.PlaceOrderForm_CreditCard,"12345");
		type(cart.PlaceOrderForm_Month,"Oct");
		type(cart.PlaceOrderForm_Year,"2020");
		return this;	
	}
	
	public CartPage clickOnPurchase() {
		click(cart.Purchase);
		return this;	
	}
}
