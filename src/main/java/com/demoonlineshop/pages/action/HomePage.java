package com.demoonlineshop.pages.action;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;


import com.demoonlineshop.base.BasePage;
import com.demoonlineshop.pages.locators.HomePageLocators;

/**
 * TODO Put here a description of what this class does.
 *
 * @author z003x39f.
 *         Created Oct 16, 2020.
 */
public class HomePage extends BasePage {

	public HomePageLocators home;
	
	
	public HomePage() {
		this.home=new HomePageLocators();
		AjaxElementLocatorFactory factory = new AjaxElementLocatorFactory(driver,10);
		PageFactory.initElements(factory, this.home);
	}
	
	
      public HomePage gotoPhonesTab(){
		click(home.PhonesTab);
		return this;
	  }
      
      public HomePage gotoLaptopTab(){
  		click(home.LaptopTab);
  		return this;
  	  }
      
      public HomePage gotoMonitorTab(){
    		click(home.MonitorTab);
    		return this;
    	  }
      
      public ProductDescriptionPage selectLaptopSonyVaioi5(){
    	  click(home.SonyVaioi5);
		return new ProductDescriptionPage();
      }	
	 
      public ProductDescriptionPage selectLaptopDelli78gb(){
	   click(home.Delli78gb);
	   return new ProductDescriptionPage();
    	  
    	  
      }


	
}
