package com.demoonlineshop.pages.action;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

import com.demoonlineshop.base.BasePage;
import com.demoonlineshop.pages.locators.TopNavigationLocators;



/**
 * TODO Put here a description of what this class does.
 *
 * @author z003x39f.
 *         Created Oct 16, 2020.
 */
public class TopNavigation {
	
public TopNavigationLocators topNavigation;
	
	public TopNavigation(WebDriver driver){
		
		this.topNavigation = new TopNavigationLocators();
		AjaxElementLocatorFactory factory = new AjaxElementLocatorFactory(driver,10);
		PageFactory.initElements(factory, this.topNavigation);
	}
	
     public CartPage gotoCart(){
		BasePage.click(topNavigation.Cart);
		return new CartPage();
		
	}
     
     public HomePage gotoHome(){
 		BasePage.click(topNavigation.Home);
 		return new HomePage();
 		
 	}
	

}
