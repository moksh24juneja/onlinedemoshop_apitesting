package com.demoonlineshop.testcases;

import org.testng.annotations.Test;

import com.demoonlineshop.base.BasePage;
import com.demoonlineshop.pages.action.CartPage;
import com.demoonlineshop.pages.action.HomePage;
import com.demoonlineshop.pages.action.ProductDescriptionPage;
import com.demoonlineshop.pages.action.TopNavigation;
import com.demoonlineshop.pages.locators.TopNavigationLocators;


/**
 * TODO Put here a description of what this class does.
 *
 * @author z003x39f. Created Oct 16, 2020.
 */
public class BasicTestCase extends BasePage {

	
	@Test
	public void EndToEndTest() {

		HomePage home = new HomePage();
		ProductDescriptionPage DescriptionPage= home.gotoLaptopTab().selectLaptopSonyVaioi5();
		TopNavigation topNavigation=DescriptionPage.AddToCart().clickOKPopUp();
		home= topNavigation.gotoHome();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException exception) {
			// TODO Auto-generated catch-block stub.
			exception.printStackTrace();
		}
		DescriptionPage=home.gotoLaptopTab().selectLaptopDelli78gb();
		topNavigation=DescriptionPage.AddToCart().clickOKPopUp();
		CartPage cart=topNavigation.gotoCart();
		cart.deleteTheProduct_LaptopDelli78gb().clickOnPlaceOrder().FillPurchaseOrderForm().clickOnPurchase();
		
	}

}
